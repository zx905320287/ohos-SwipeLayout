﻿# OhosSwipeLayout

## 简介
> 支持上、下、左、右四个方向的滑动布局。

![sample.gif](sample.gif)

## 下载安装
```shell
npm install @ohos/swipelayout --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### 在build中使用()
```
  Column() {
    SwipeLayoutLayDown({
              bottomViewLeftW: 200,
              bottomViewRightW: 300,
              bottomViewTopH：60,
              bottomViewBottomH：80,
              mWidth: '100%',
              mHeight: 100,
              parentIsTouch: $isTouch
            })
      }
```
## 接口说明
`自定义容器是通过在构造方法中使用字段来设置各种属性`
1. 设置左侧布局宽度
   `bottomViewLeftW`
2. 设置右侧布局宽度
   `bottomViewRightW`
3. 设置组件高度
   `mHeight`
4. 设置组件宽度
   `mWidth`
5. 设置动画时长
   `mDuration`
6. 设置顶部布局高度
   `bottomViewTopH`
7. 设置底部布局高度
   `bottomViewBottomH`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- SwipeLayout  
|     |---- entry  # 示例代码文件夹
|     |---- SwipeLayout  # swipeLayout库文件夹
          |----src
              |----main
                  |----ets
                      |---- SwipeLayout.ets   # 自定义容器
                      |---- SimpleSwipeListener.ets   # 滑动监听器
                      |---- util/Attributes.ets   # 滑动模式枚举类
                      |---- SwipeLayoutLayDown.ets   # LayDown滑动效果的自定义容器
                      |---- SwipeLayoutLayDownRoot.ets   # 可以嵌套容器的自定义容器
                      |---- SwipeLayoutPullOut.ets   # PullOut滑动效果的自定义容器
                      |---- SwipeLayoutLayDownFullGrid.ets   # 适用于Grid自定义容器   
                      |---- SwipeLayoutLayDownFullList.ets   # 适用于List自定义容器 
|          |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-SwipeLayout/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos-SwipeLayout/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-SwipeLayout/blob/master/LICENSE) ，请自由地享受和参与开源。
