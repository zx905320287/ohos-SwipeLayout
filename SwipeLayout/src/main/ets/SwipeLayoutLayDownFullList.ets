/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import prompt from '@system.prompt';

@Component
export struct SwipeLayoutLayDownFullList {
  private mDuration= 200
//  @BuilderParam surfaceView: () => {};
//  @BuilderParam bottomViewRight: () => {};
//  @BuilderParam bottomViewLeft: () => {};
//  @BuilderParam bottomViewTop: () => {};
//  @BuilderParam bottomViewBottom: () => {};
  private sX: number ;
  private sY: number ;
  private mWidth = '100%'
  private mHeight = 90
  private bottomViewRightW: number = 320
  private bottomViewLeftW: number = 120
  private bottomViewTopH: number = 100
  private bottomViewBottomH: number = 100
  private itemContent: string
  @State private offsetX: number= 0
  @State private offsetY: number= 0
  private rightSlideEnable= true;
  private leftSlideEnable= true;
  private previousX = 0;
  private topState;
  private enableSlide: boolean[]= new Array(false, true, true, false)

  build() {
    Column() {
      Row() {
        Column() {
          Text(this.itemContent)
            .fontSize('16')
            .width('100%')
            .height(this.mHeight)
            .fontColor('#000000')
            .padding(5)
            .onClick(() => {
              prompt.showToast({
                message: "Click on surface",
                duration: 1500,
              });
            })
            .backgroundColor('#ffffff')
        }

        .onTouch((event: TouchEvent) => {
          if (event.type === TouchType.Down) {
            this.sX = event.touches[0].screenX
            this.sY = event.touches[0].screenY
          }

          if (event.type === TouchType.Move) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            if (Math.abs(moveX) > Math.abs(moveY)) { // 横向滑动
              if (this.offsetY == 0) {
                if (moveX < 0 && moveY < 30 && moveY > -30) { //左滑
                  //左侧菜单处于打开状态，开始关闭
                  if (this.offsetX > 0) {
                    this.rightSlideEnable = false
                    if (-moveX < this.bottomViewLeftW) {
                      this.offsetX = moveX + this.bottomViewLeftW
                    } else {
                      this.offsetX = 0
                    }
                    return
                  }


                  if (!this.enableSlide[2]) {
                    return
                  }
                  //当前移动距离小于右侧View的宽度，并且没有处于刚好拉出的状态
                  if (this.rightSlideEnable) {
                    if (this.offsetX <= 0 && this.offsetX > -this.bottomViewRightW && -moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX
                    } else if (-moveX > this.bottomViewRightW) {
                      this.offsetX = -this.bottomViewRightW
                      this.sX = event.touches[0].screenX
                      this.leftSlideEnable = false
                    }
                  }

                } else if (moveX > 0 && moveY < 30 && moveY > -30) { //右滑
                  //右侧菜单开始关闭
                  if (this.offsetX < 0) {
                    this.leftSlideEnable = false
                    if (moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX - this.bottomViewRightW
                      return
                    } else {
                      this.offsetX = 0
                    }
                  }

                  if (!this.enableSlide[0]) {
                    return
                  }
                  //当前移动距离小于左侧View的宽度  并且没有处于刚好拉出的状态
                  if (this.leftSlideEnable) {
                    if (this.offsetX >= 0 && this.offsetX < this.bottomViewLeftW && moveX <= this.bottomViewLeftW) {
                      this.offsetX = moveX
                    } else if (moveX > this.bottomViewRightW) {
                      this.offsetX = this.bottomViewLeftW
                      this.sX = event.touches[0].screenX
                      this.rightSlideEnable = false
                    }
                  }
                }
              }
            }
            this.previousX = event.touches[0].screenX
          }

          if (event.type === TouchType.Up) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            animateTo({
              duration: this.mDuration, // 动画时长
              tempo: 1, // 播放速率
              curve: Curve.FastOutLinearIn, // 动画曲线
              iterations: 1, // 播放次数
              playMode: PlayMode.Normal, // 动画模式
              onFinish: () => {
                this.rightSlideEnable = true
                this.leftSlideEnable = true
              }
            }, () => {
              if (this.offsetX > 0 && this.leftSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = this.bottomViewLeftW
                } else if (moveX > 0) {
                  this.offsetX = 0
                }
                if (moveX < -40) {
                  this.offsetX = 0
                } else if (moveX < 0) {
                  this.offsetX = this.bottomViewLeftW
                }
                return
              }

              if (this.offsetX < 0 && this.rightSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = 0
                } else if (moveX > 0) {
                  this.offsetX = -this.bottomViewRightW
                }

                if (moveX <= -40) {
                  this.offsetX = -this.bottomViewRightW
                } else if (moveX < 0) {
                  this.offsetX = 0
                }
                return
              }

              if (this.offsetY > 0) {
                if (moveY >= 20) {
                  this.offsetY = this.bottomViewTopH
                } else {
                  this.offsetY = 0
                }
                return
              }

              if (this.offsetY < 0) {
                if (moveY <= -20) {
                  this.offsetY = -this.bottomViewBottomH
                } else {
                  this.offsetY = 0
                }
                return
              }

            })
          }
        })


        Column() {
          Row() {
            Image($r('app.media.trash'))
              .width('10%')
              .height('100%')
              .padding(8)
              .objectFit(ImageFit.Contain)
              .onClick(() => {
                prompt.showToast({
                  message: "Magnifier",
                  duration: 1500,
                });
              })


            Text('Delete item?')
              .fontSize('14')
              .height(this.mHeight)
              .fontColor('#ffffff')
              .padding(5)
              .onClick(() => {
                prompt.showToast({
                  message: "Click on surface",
                  duration: 1500,
                });
              })

            Blank()

            Button('Yes,Delete')
              .backgroundColor('#ffffff')
              .fontColor('#e52626')
              .fontSize(14)
              .width('35%')
              .type(ButtonType.Normal)
              .onClick((event: ClickEvent) => {
                prompt.showToast({ message: 'click delete', duration: 1500 })
              })
          }
          .height(this.mHeight)
          .width(this.bottomViewRightW)

        }

        .onTouch((event: TouchEvent) => {
          if (event.type === TouchType.Down) {
            this.sX = event.touches[0].screenX
            this.sY = event.touches[0].screenY
          }

          if (event.type === TouchType.Move) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            if (Math.abs(moveX) > Math.abs(moveY)) { // 横向滑动
              if (this.offsetY == 0) {
                if (moveX < 0 && moveY < 30 && moveY > -30) { //左滑
                } else if (moveX > 0 && moveY < 30 && moveY > -30) { //右滑
                  if (this.offsetX < 0) {
                    this.leftSlideEnable = false
                    if (moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX - this.bottomViewRightW
                      return
                    } else {
                      this.offsetX = 0
                    }
                  }
                }
              }
            }
            this.previousX = event.touches[0].screenX
          }

          if (event.type === TouchType.Up) {
            let moveX = event.touches[0].screenX - this.sX;
            animateTo({
              duration: this.mDuration, // 动画时长
              tempo: 1, // 播放速率
              curve: Curve.FastOutLinearIn, // 动画曲线
              iterations: 1, // 播放次数
              playMode: PlayMode.Normal, // 动画模式
              onFinish: () => {
                this.rightSlideEnable = true
                this.leftSlideEnable = true
              }
            }, () => {
              if (this.offsetX < 0 && this.rightSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = 0
                } else if (moveX > 0) {
                  this.offsetX = -this.bottomViewRightW
                }

                if (moveX <= -40) {
                  this.offsetX = -this.bottomViewRightW
                } else if (moveX < 0) {
                  this.offsetX = 0
                }
                return
              }
            })
          }
        })
        .backgroundColor('#FF5534')
      }
    }
    .width(this.mWidth)
    .height(this.mHeight)
    .position({ x: this.offsetX, y: this.offsetY })
  }
}