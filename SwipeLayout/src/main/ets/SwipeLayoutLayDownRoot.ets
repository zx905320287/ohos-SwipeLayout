/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//import {SwipeLayout} from '../common/SwipeLayout.ets'
import prompt from '@system.prompt';
import {SwipeLayoutPullOut} from '../ets/SwipeLayoutPullOut'
import {SwipeLayoutLayDown} from '../ets/SwipeLayoutLayDown'
import {SwipeLayoutLayDownFull} from '../ets/SwipeLayoutLayDownFull'

@Component
export struct SwipeLayoutLayDownRoot {
  @State isTouch: boolean = true
  private mDuration= 200
//  @BuilderParam surfaceView: () => {};
//  @BuilderParam bottomViewRight: () => {};
//  @BuilderParam bottomViewLeft: () => {};
//  @BuilderParam bottomViewTop: () => {};
//  @BuilderParam bottomViewBottom: () => {};
  private sX: number ;
  private sY: number ;
  private mWidth = '100%'
  private mHeight = 90
  private bottomViewRightW: number = 120
  private bottomViewLeftW: number = 120
  private bottomViewTopH: number = 120
  private bottomViewBottomH: number = 720
  @State private offsetX: number= 0
  @State private offsetY: number= 0
  private rightSlideEnable= true;
  private leftSlideEnable= true;
  private previousX = 0;
  private topState

  build() {
    Column() {
      Column() {
        Image($r('app.media.bird'))
          .padding(12)
          .objectFit(ImageFit.Contain)
          .backgroundColor('#ffffff')
          .onClick(() => {
            prompt.showToast({
              message: "click search",
              duration: 1500,
            });
          })
          .height(this.bottomViewTopH)
          .width('100%')
      }
      .position({ x: 0, y: -this.bottomViewTopH })
      .onTouch((event: TouchEvent) => {
        if (event.type === TouchType.Down) {
          this.sX = event.touches[0].screenX
          this.sY = event.touches[0].screenY
        }

        if (event.type === TouchType.Move) {
          let moveX = event.touches[0].screenX - this.sX;
          let moveY = event.touches[0].screenY - this.sY;
          if (Math.abs(moveX) < Math.abs(moveY)) { //纵向滑动
            if (moveY < 0) { //上滑
              //顶侧菜单开始关闭
              if (this.offsetY > 0) {
                if (this.mHeight - Math.abs(moveY) >= 0) {
                  this.offsetY = this.bottomViewTopH + moveY
                  return
                }
              }
            }
          }
        }

        if (event.type === TouchType.Up) {
          let moveY = event.touches[0].screenY - this.sY;
          if (moveY >= 0) {
            return
          }
          animateTo({
            duration: this.mDuration, // 动画时长
            tempo: 1, // 播放速率
            curve: Curve.FastOutLinearIn, // 动画曲线
            iterations: 1, // 播放次数
            playMode: PlayMode.Normal, // 动画模式
          }, () => {
            if (this.offsetY > 0) {
              if (Math.abs(moveY) >= 20) {
                this.offsetY = 0
              } else {
                this.offsetY = this.bottomViewTopH
              }
            }
          })
        }
      })

      Row() {
        Column() {
          Image($r('app.media.bird'))
            .backgroundColor('#ffffff')
            .padding(12)
            .objectFit(ImageFit.Contain)
            .onClick(() => {
              prompt.showToast({
                message: "click search",
                duration: 1500,
              });
            })
            .height(this.mHeight)
            .width(this.bottomViewLeftW)
        }
        .position({ x: -this.bottomViewLeftW, y: 0 })

        Column() {
          ColumnSplit() {
            SwipeLayoutLayDown({
              bottomViewLeftW: this.bottomViewLeftW,
              bottomViewRightW: this.bottomViewRightW,
              mWidth: '100%',
              mHeight: 100,
              parentIsTouch: $isTouch
            })
          }
          .backgroundColor('#e3ee5a')

          ColumnSplit() {
            SwipeLayoutPullOut({
              bottomViewLeftW: this.bottomViewLeftW,
              bottomViewRightW: this.bottomViewRightW,
              mWidth: '100%',
              mHeight: 100,
              parentIsTouch: $isTouch
            })
          }
          .backgroundColor('#f04f97')

          ColumnSplit() {
            SwipeLayoutLayDownFull({
              bottomViewLeftW: this.bottomViewLeftW,
              bottomViewRightW: 720,
              mWidth: '100%',
              mHeight: 100,
              parentIsTouch: $isTouch
            })
          }
          .backgroundColor('#e3ee5a')

          Image($r('app.media.hand'))
            .backgroundColor('#ffffff')
            .objectFit(ImageFit.Contain)
            .onClick(() => {
              prompt.showToast({
                message: "click search",
                duration: 1500,
              });
            })
            .height(90)
            .width(90)
            .margin({ top: 40 })
        }
        .height('100%').width('100%').backgroundColor('#ffffff')
        .onTouch((event: TouchEvent) => {
          if (!this.isTouch) {
            return
          }
          if (event.type === TouchType.Down) {
            this.sX = event.touches[0].screenX
            this.sY = event.touches[0].screenY
          }

          if (event.type === TouchType.Move) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            if (Math.abs(moveX) > Math.abs(moveY)) { // 横向滑动
              if (this.offsetY == 0) {
                if (moveX < 0 && moveY < 30 && moveY > -30) { //左滑
                  //左侧菜单处于打开状态，开始关闭
                  if (this.offsetX > 0) {
                    this.rightSlideEnable = false
                    if (-moveX < this.bottomViewLeftW) {
                      this.offsetX = moveX + this.bottomViewLeftW
                    } else {
                      this.offsetX = 0
                    }
                    return
                  }

                  //当前移动距离小于右侧View的宽度，并且没有处于刚好拉出的状态
                  if (this.rightSlideEnable) {
                    if (this.offsetX <= 0 && this.offsetX > -this.bottomViewRightW && -moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX
                    } else if (-moveX > this.bottomViewRightW) {
                      this.offsetX = -this.bottomViewRightW
                      this.sX = event.touches[0].screenX
                      this.leftSlideEnable = false
                    }
                  }

                } else if (moveX > 0 && moveY < 30 && moveY > -30) { //右滑
                  //右侧菜单开始关闭
                  if (this.offsetX < 0) {
                    this.leftSlideEnable = false
                    if (moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX - this.bottomViewRightW
                      return
                    } else {
                      this.offsetX = 0
                    }
                  }

                  //当前移动距离小于左侧View的宽度  并且没有处于刚好拉出的状态
                  if (this.leftSlideEnable) {
                    if (this.offsetX >= 0 && this.offsetX < this.bottomViewLeftW && moveX <= this.bottomViewLeftW) {
                      this.offsetX = moveX
                    } else if (moveX > this.bottomViewRightW) {
                      this.offsetX = this.bottomViewLeftW
                      this.sX = event.touches[0].screenX
                      this.rightSlideEnable = false
                    }
                  }
                }
              }
            } else { //纵向滑动
              if (this.offsetX == 0) {
                if (moveY > 0) { //下滑
                  //底部菜单开始关闭
                  if (this.offsetY < 0) {
                    if (moveY - this.bottomViewBottomH <= 0) {
                      this.offsetY = moveY - this.bottomViewBottomH
                      return
                    }
                  }
                  //当前移动距离小于右侧View的宽度，并且没有处于刚好拉出的状态
                  if (this.offsetY < this.bottomViewTopH) {
                    this.offsetY = moveY
                  } else {
                    this.offsetY = this.bottomViewTopH
                  }
                } else if (moveY < 0) { //上滑
                  //顶侧菜单开始关闭
                  if (this.offsetY > 0) {
                    if (this.bottomViewTopH - Math.abs(moveY) >= 0) {
                      this.offsetY = this.bottomViewTopH + moveY
                      return
                    }
                  }

                  if (this.offsetY <= 0) {
                    if (Math.abs(this.offsetY) < this.bottomViewBottomH) {
                      this.offsetY = moveY
                    } else {
                      this.offsetY = -this.bottomViewBottomH
                    }
                  }
                }
              }
            }
            this.previousX = event.touches[0].screenX
          }

          if (event.type === TouchType.Up) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            animateTo({
              duration: this.mDuration, // 动画时长
              tempo: 1, // 播放速率
              curve: Curve.FastOutLinearIn, // 动画曲线
              iterations: 1, // 播放次数
              playMode: PlayMode.Normal, // 动画模式
              onFinish: () => {
                this.rightSlideEnable = true
                this.leftSlideEnable = true
              }
            }, () => {
              if (this.offsetX > 0 && this.leftSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = this.bottomViewLeftW
                } else if (moveX > 0) {
                  this.offsetX = 0
                }
                if (moveX < -40) {
                  this.offsetX = 0
                } else if (moveX < 0) {
                  this.offsetX = this.bottomViewLeftW
                }
                return
              }

              if (this.offsetX < 0 && this.rightSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = 0
                } else if (moveX > 0) {
                  this.offsetX = -this.bottomViewRightW
                }

                if (moveX <= -40) {
                  this.offsetX = -this.bottomViewRightW
                } else if (moveX < 0) {
                  this.offsetX = 0
                }
                return
              }

              if (this.offsetY > 0) {
                if (moveY >= 20) {
                  this.offsetY = this.bottomViewTopH
                } else {
                  this.offsetY = 0
                }
                return
              }

              if (this.offsetY < 0) {
                if (moveY <= -20) {
                  this.offsetY = -this.bottomViewBottomH
                } else {
                  this.offsetY = 0
                }
                return
              }
            })
          }
        })


        Column() {
          Image($r('app.media.bird'))
            .backgroundColor('#ffffff')
            .padding(12)
            .objectFit(ImageFit.Contain)
            .onClick(() => {
              prompt.showToast({
                message: "click search",
                duration: 1500,
              });
            })
            .height(this.mHeight)
            .width(this.bottomViewRightW)
        }
      }

      Column() {
        Image($r('app.media.bird'))
          .backgroundColor('#ffffff')
          .padding(12)
          .objectFit(ImageFit.Contain)
          .onClick(() => {
            prompt.showToast({
              message: "click search",
              duration: 1500,
            });
          })
          .height(this.bottomViewBottomH)
          .width('100%')
      }
      .onTouch((event: TouchEvent) => {
        if (event.type === TouchType.Down) {
          this.sX = event.touches[0].screenX
          this.sY = event.touches[0].screenY
        }

        if (event.type === TouchType.Move) {
          let moveX = event.touches[0].screenX - this.sX;
          let moveY = event.touches[0].screenY - this.sY;
          if (Math.abs(moveX) < Math.abs(moveY)) { //纵向滑动
            if (moveY > 0) { //下滑
              //底部菜单开始关闭
              if (this.offsetY < 0) {
                if (this.mHeight - Math.abs(moveY) >= 0) {
                  this.offsetY = -(this.bottomViewBottomH - moveY)
                }
              }
            }
          }
        }

        if (event.type === TouchType.Up) {
          let moveY = event.touches[0].screenY - this.sY;
          if (moveY <= 0) {
            return
          }
          animateTo({
            duration: this.mDuration, // 动画时长
            tempo: 1, // 播放速率
            curve: Curve.FastOutLinearIn, // 动画曲线
            iterations: 1, // 播放次数
            playMode: PlayMode.Normal, // 动画模式
          }, () => {
            if (this.offsetY < 0) {
              if (Math.abs(moveY) >= 20) {
                this.offsetY = 0
              } else {
                this.offsetY = -this.bottomViewBottomH
              }
            }
          })
        }
      })
    }
    .width(this.mWidth)
    .height(this.mHeight)
    .position({ x: this.offsetX, y: this.offsetY })
  }
}